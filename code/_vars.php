<?php

    $urlImagePlaceholder =  $_ENV['URL_PLACEHOLDER_IMAGE'] ?? 
    	"https://pipeline-museumapp.naturalis.nl/stubs/placeholder.jpg";
    $urlObjectPlaceholder = $_ENV['URL_PLACEHOLDER_OBJECT_IMAGE'] ?? 
    	"https://pipeline-museumapp.naturalis.nl/stubs/object-placeholder.jpg";
    $urlSquaredImageRoot = $_ENV['URL_SQUARES_IMAGE_ROOT'] ?? 
    	"https://pipeline-museumapp.naturalis.nl/squared_images/";
    $urlLeenImageRoot = $_ENV['URL_LEENOBJECTEN_IMAGE_ROOT'] ?? 
    	"https://pipeline-museumapp.naturalis.nl/leenobject_images/";    

    $autoTranslationText_EN = $_ENV['AUTO_TRANSLATION_TEXT'] ?? 
    	"(this text was translated automatically)";

    $data_brahmsPrefixes = isset($_ENV['DATA_BRAHMS_PREFIXES']) ?
        json_decode($_ENV['DATA_BRAHMS_PREFIXES']) : [];

    $data_galleriesEnglishNames = isset($_ENV['DATA_GALLERIES_ENGLISH_NAMES']) ? 
    	json_decode($_ENV['DATA_GALLERIES_ENGLISH_NAMES'], true) : [];

    $data_exhibitionRoomsPublic = isset($_ENV['DATA_GALLERIES_NATUURWIJZER_TO_LABEL']) ? 
    	json_decode($_ENV['DATA_GALLERIES_NATUURWIJZER_TO_LABEL'], true) : [];    

    $data_exhibitionRoomsTranslations = isset($_ENV['DATA_GALLERIES_SHEET_TO_NATUURWIJZER']) ? 
    	json_decode($_ENV['DATA_GALLERIES_SHEET_TO_NATUURWIJZER'], true) : [];

    $data_roomsToMatchLinksOn = isset($_ENV['DATA_GALLERIES_TO_MATCH_LINKS_ON']) ? 
    	json_decode($_ENV['DATA_GALLERIES_TO_MATCH_LINKS_ON']) : [];

    $data_IUCN_statusTranslations = isset($_ENV['DATA_IUCN_STATUSTRANSLATIONS']) ? 
    	json_decode($_ENV['DATA_IUCN_STATUSTRANSLATIONS'], true) : [];

    $data_ttikCategoryNames = isset($_ENV['DATA_TTIK_CATEGORY_TO_LABEL']) ? 
    	json_decode($_ENV['DATA_TTIK_CATEGORY_TO_LABEL'], true) : [];
